# Breast Cancer Detection Using ML
# By- Aarush Kumar
Model to detect breast cancer using various ML algorithms such as LogisticRegression, DecisionTree & RandomForestClassifier. 
Breast cancer is cancer that forms in the cells of the breasts.
After skin cancer, breast cancer is the most common cancer diagnosed in women and is constantly increasing all aound the world.
The doctors do not identify each and every breast cancer patient. That’s the reason Machine Learning Engineer / Data Scientist comes into the picture because they have knowledge of maths and computational power. Hence this is the place where ML comes into play.In this project I have extracted features of breast cancer patient cells and normal person cells i.e. M stands for malignant cell(having high risk to cancer) and B stands for benign cells which shows whether a person has cancer or not. As a Machine learning engineer / Data Scientist has to create an ML model to classify malignant and benign tumor.So that it may be stopped from growing at an early stage. To complete this ML project the steps followed are as under:
1. Import essential libraries
2. Load breast cancer dataset & explore
3. Create DataFrame
4. Data Visualization
5. Counterplot
6. Data Preprocessing
7. Feature Scaling
8. Building Model
9. Detecting & producing outputs

Thankyou..
